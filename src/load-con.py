""" Текстовый клиент для проверки коннектора """
import argparse
from twisted.internet import reactor, task
from pv.connector.twisted import TwistedTransport
from pv.connector.base import Connector, ConnectorConfig, Transport

SUCCESS = 0
ICNT = 0
FCNT = 0


def empty(arg1=None):
    """ Функция-затычка для пустых обработчиков событий"""
    pass


def handshake(connector):
    """ Рукопожатие. Всегда должно выполняться первым """
    connector.hello()


def simple_int_loader(connector, size=1):
    """ Генератор интовых значений """
    global SUCCESS
    global ICNT

    for i in range(size):
        result = connector.set('demo.int_tag', ICNT, 0)
        if result:
            SUCCESS += 1
        ICNT += 1


def simple_float_loader(connector, size=1):
    """ Генератор значений с точкой"""
    global SUCCESS
    global FCNT

    for i in range(size):
        result = connector.set('demo.float_tag', FCNT + 0.123, 0)
        if result:
            SUCCESS += 1
        FCNT += 1


def main():
    """
    - разобрать аргументы
    - запустить коннектор
    - запустить генератора
    - запустить twisted
    Дубовый генератор значений, чтобы можно было по-быстрому выполнить
    профилирование VC.
    Тупо подключается, тупо генерит пачки значений. Нет никаких переподключений
    и сложных операций -> VC должен быть запущен до того, как запустится
    генератор.
    """
    global SUCCESS
    global ICNT
    global FCNT
    SUCCESS = 0
    ICNT = 0
    FCNT = 0
    CHUNK_SIZE = 10
    CONNECT_TIMEOUT = 0.5
    GEN_TIMEOUT = 0.5
    parser = argparse.ArgumentParser(description='Noise generator')

    parser.add_argument('--host',
                        dest='host',
                        default='127.0.0.1',
                        help='VC address',
                        )
    parser.add_argument('--port',
                        dest='port',
                        type=int,
                        default=8091,
                        help='VC port',
                        )

    args = parser.parse_args()

    # Конфигурация коннектора
    config = ConnectorConfig()
    config.name = 'PeavyLoader'
    config.host = args.host
    config.port = args.port

    # Настройка транспортного уровня коннектора
    config.transport = Transport(
        TwistedTransport, reactor, config.host, config.port)

    # Настройка обработчиков событий
    config.events.up = empty
    config.events.down = empty
    config.events.echo = empty

    # Создание коннектора
    connector = Connector(config)
    connector.start()

    # Создать задачу для handshak'а
    reactor.callLater(CONNECT_TIMEOUT, handshake, connector)

    # Создание циклов для генерации
    int_loop = task.LoopingCall(
        lambda: simple_int_loader(connector, CHUNK_SIZE))
    int_loop.start(CONNECT_TIMEOUT + GEN_TIMEOUT)

    float_loop = task.LoopingCall(
        lambda: simple_float_loader(connector, CHUNK_SIZE))
    float_loop.start(CONNECT_TIMEOUT + GEN_TIMEOUT)

    # Работать отсюда и до обеда
    reactor.run()
    print('SUCCESS: {0}'.format(SUCCESS))


if __name__ == '__main__':
    main()
