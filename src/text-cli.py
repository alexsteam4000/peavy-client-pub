
import argparse
from twisted.internet import reactor, stdio
from twisted.protocols.basic import LineReceiver


from pv.connector.transport import Transport
from pv.connector.twisted import TwistedTransport
from pv.client.timer import Timers
from pv.client.twisted import TwistedTimerFactory
from pv.client.base import Client, ClientEvents, ClientConfig


class Commands(LineReceiver):
    """ Обработчик команд пользователя """

    def __init__(self, client):
        super().__init__()
        self.client = client
        self.supported = {
            'find': self.client.find,
            'sub': self.client.subscribe,
            'set': self.client.set,
            'exit': self.exit,
            'detach': self.detach,
            'start': self.client.start,
            'stop': self.client.stop,
        }

    def connectionMade(self):
        """ Терминал запущеню Выводим приглашение """
        self.delimiter = b'\n'
        self.transport.write(b'>>> ')

    def lineReceived(self, line):
        """ Строка прочитана. Отрабатываем и выводим приглашение """
        self.execute_command(line)
        self.transport.write(b'>>> ')

    def list(self):
        """ Список команд """
        l = list()
        for k in self.supported.keys():
            l.append(k)
        return l

    def execute_command(self, line):
        """ Отработка команд
        Сводится к поиску в словаре поддерживаемых команд и вызову с аргументами
        пользовательского ввода
        """
        result = 0
        args = None
        tokens = line.split()
        if not tokens:
            return

        cmd = tokens[0].decode('utf-8')
        if len(tokens) > 1:
            args = list(map(lambda p: Commands.convert(p), tokens[1:]))

        if cmd not in self.supported:
            print('Unknown command')
            return

        try:
            if args:
                if len(args) == 1:
                    result = self.supported[cmd](args)
                else:
                    result = self.supported[cmd](*args)
            else:
                result = self.supported[cmd]()
#            result = self.supported[cmd](
#                args) if args else self.supported[cmd]()
        except Exception as e:
            print(e)

        print('Execute command with result: {0}'.format(result))

    @staticmethod
    def convert(text):
        """ Конвертация 'особых' слов
        К ним относятся true/false и числа
        """
        try:
            fp = text.find(b'.')
            if fp == -1:
                v = int(text)
            else:
                v = float(text)
            return v
        except:
            pass

        if text in [b'True', b'true']:
            return True

        if text in [b'False', b'false']:
            return False

        return text.decode('utf-8')

    def detach(self):
        """ Отсоединить терминал
        После отсоединения ввод новых команд будет невозможен.
        Позволяет избежать блокировки при переполнении stdin'а
        """
        self.transport.write(
            b'STDIN detached. Restart app to get opportunity to enter new commands\n')
        self.transport.loseConnection()
        return True

    def exit(self):
        """ Выйти """
        reactor.stop()
        return True


def main():
    """
    - разобрать аргументы
    - запустить коннектор
    - запустить работу с терминалом
    - запустить twisted
    """
    parser = argparse.ArgumentParser(description='VC text client')

    parser.add_argument('--host',
                        dest='host',
                        default='127.0.0.1',
                        help='VC address',
                        )
    parser.add_argument('--port',
                        dest='port',
                        type=int,
                        default=8091,
                        help='VC port',
                        )

    args = parser.parse_args()

    cfg = ClientConfig()

    # Дополнить настройки и создать клиента VC
    cfg.host = args.host
    cfg.port = args.port
    cfg.events = ClientEvents()
    cfg.transport = Transport(
        TwistedTransport,
        reactor,
        cfg.host,
        cfg.port,
        cfg.reconnect_timeout)
    cfg.timers = Timers(TwistedTimerFactory, reactor)
    cfg.name = "PeavyTextClient"

    # Привязать обработчики событий
    # Оброаботчик успешного рукопожатия (коннектор готов к работе)
    cfg.events.up = lambda: print('Connected')
    cfg.events.down = lambda: print('Disconnected')

    cfg.events.find = lambda res: print(
        'FIND:\nResult={0}\nConfirmed={1}'.format(res.success, res.result))

    cfg.events.subscribe = lambda res: print(
        'SUBS:\nResult={0}\nConfirmed={1}'.format(res.success, res.result))

    cfg.events.update = lambda res: print(
        'UPDATE:\nResult={0}\nData={1}'.format(res.success, res.result))

    # Запуск клиента
    client = Client(cfg)

    # Работа с терминалом
    commands = Commands(client)

    print("Supported commands:")
    for c in commands.list():
        print("\t {0}".format(c))

    stdio.StandardIO(commands)

    reactor.run()


if __name__ == '__main__':
    main()


# Пример работы
# python text-cli.py
# >>> start
# Execute command with result: None
# >>> Connected
#
# >>> sub demo.int_tag
# Execute command with result: True
# >>> SUBSDONE
# SUBS:
# Result=True
# Confirmed={'confirmed': ['demo.int_tag'], 'rejected': 'Not supported in proto v.0.2.0'}
# UPDATE:
# Result=True
# Data={'tag': 'demo.int_tag', 'v': None, 't': '2019-01-18T09:20:05.682421', 'q': 2}
#
# >>> find dd
# Execute command with result: True
# >>> FIND:
# Result=False
# Confirmed={'confirmed': [], 'rejected': [('dd', None)]}
#
# >>> find demo.int_tag
# Execute command with result: True
# >>> FIND:
# Result=True
# Confirmed={'confirmed': [('demo.int_tag', 'int')], 'rejected': []}
#
# >>> find dd
# Execute command with result: True
# >>> FIND:
# Result=False
# Confirmed={'confirmed': [], 'rejected': [('dd', None)]}
#
# >>> set demo.int_tag 22 1
# Execute command with result: True
# >>> UPDATE:
# Result=True
# Data={'tag': 'demo.int_tag', 'v': 22, 't': '2019-01-18T09:42:46.146428', 'q': 1}
#
# >>> set demo.int_tag 333 1
# Execute command with result: True
# >>> UPDATE:
# Result=True
# Data={'tag': 'demo.int_tag', 'v': 333, 't': '2019-01-18T09:42:55.261063', 'q': 1}
#
# >>> set demo.int_tag 333 1
# Execute command with result: True
#
