""" Моудль для реализации клиента VC
В отличие от коннектора, клиент реализует более высокоуровневые операции,
такие как подписка на множество тэгов, контроль связи, рукопожатие при коннекте
и т.п. """
import time
from pv.connector.base import Connector, ConnectorConfig
from pv.connector.message import MessageHelper
from pv.client.task import Task, TaskNoop, TaskExecutor, TaskEcho, TaskFind, TaskSubscribe, TaskHandshake


class EchoScheduler:
    """ Планировщик для проверки связи через echo """

    def __init__(self, timeout):
        self._last_activity = 0
        self._timeout = timeout

    def activity(self):
        """ Отметить активность """
        self._last_activity = time.monotonic()

    def expired(self):
        """ Не пора ли послать echo """
        return abs(time.monotonic() - self._last_activity) > self._timeout


class EventResult:
    """ Результат после наступления события  """

    def __init__(self, success, **kwargs):
        self.success = success
        self.result = {**kwargs}


class ClientEvents:
    """ События в клиенте """

    def __init__(self):
        def empty0():
            pass

        def empty1(event_res):
            pass

        # Связь установленаo
        self.handshake = empty0

        # Коннект поднят/упал
        self.up = empty0
        self.down = empty0

        # Поиск тэгов завершен/очередной тэг найден
        self.find = empty1

        # Подписка на тэги завершена
        self.subscribe = empty1

        # Тэг изменен
        self.update = empty1


class ClientConfig:
    """ Конфигурация клиента """

    def __init__(self):
        self.name = None
        self.host = None
        self.port = None
        self.operation_timeout = 5
        self.reconnect_timeout = 30
        self.echo_timeout = 30
        self.timers = None
        self.transport = None
        self.events = None

    def validate(self):
        return self.name and self.host and self.port


class Client:
    """ Клиент VC """

    def __init__(self, config):

        assert config
        assert config.events

        self._ready = False
        self._timeout = config.operation_timeout

        # Фабрика для таймеров
        self._timers = config.timers

        # Задачи клиента
        self._subscribe = TaskExecutor(self._timers, 0, TaskNoop())
        self._echo = TaskExecutor(self._timers, 0, TaskNoop())
        self._handshake = TaskExecutor(self._timers, 0, TaskNoop())
        self._find = TaskExecutor(self._timers, 0, TaskNoop())

        # Цикл для ECHO
        self._echo_loop = TaskExecutor(self._timers, 0, TaskNoop())
        self._echo_sched = EchoScheduler(config.echo_timeout)

        # События с верхнего уровня
        self._events = config.events

        # Конфигурация для коннектора
        cc = ConnectorConfig()
        cc.name = config.name
        cc.host = config.host
        cc.port = config.port
        cc.transport = config.transport

        # Обработчики событий из коннектора
        cc.events.hello = self._hello_handler
        cc.events.echo = self._echo_handler
        cc.events.update = self._update_handler
        cc.events.find = self._find_handler
        cc.events.up = self._link_up_handler
        cc.events.down = self._link_down_handler

        # Коннектор
        self._connector = Connector(cc)

    def start(self):
        """ Запуск клиента"""
        self._connector.start()

    def stop(self):
        """ Остановить клиента """
        self._link_down_handler()
        self._connector.stop()

    def ready(self):
        """ Признак готовности """
        return self._ready

    def subscribe(self, tags):
        """ Подписаться на тэги
        tags - список тэгов
        """
        if not self.ready():
            return False

        if self._subscribe.is_running():
            return False

        task = Task(TaskSubscribe,
                    self._connector,
                    tags,
                    self._subscribe_done)

        self._subscribe = TaskExecutor(self._timers,
                                       self._timeout,
                                       task)

        return True

    def set(self, tag, value, quality, tstamp=None):
        """ Задать значение тэга """
        if not self.ready():
            return False

        self._connector.set(tag, value, quality, tstamp)
        return True

    @property
    def connector(self):
        """ Получить доступ к коннектору """
        return self._connector

    @property
    def task_find(self):
        """ Получить доступ к задаче 'поиск тэгов'"""
        if self.find:
            return self._find.task
        return None

    @property
    def task_subscribe(self):
        """ Получить доступ к задаче 'подписка на тэги'"""
        if self.subscribe:
            return self._subscribe.task
        return None

    # ----------------------------------------
    # Уведомления о состоянии подкючения
    # ----------------------------------------
    def _link_up_handler(self):
        """ Подключение по TCP установлено """
        task = Task(TaskHandshake,
                    self._connector,
                    self._handshake_done)

        self._handshake = TaskExecutor(self._timers,
                                       self._timeout,
                                       task)

        self._events.up()

    def _link_down_handler(self):
        """ Подключение по TCP разорвано """

        self._echo_loop.stop()

        self._handshake.stop()
        self._echo.stop()
        self._find.stop()
        self._subscribe.stop()

        self._ready = False
        self._events.down()

    # ----------------------------------------
    # Уведомления о готовности к работе
    # ----------------------------------------
    def _handshake_done(self):
        """ Рукопожатие выполнено или наступил таймаут """
        task = self._handshake.task.current
        success = task.success
        if not success:
            self._connector.disconnect()
        else:
            self._echo_sched.activity()
            self._echo_loop = self._timers.timer(1, self._echo_send, False)
            self._ready = True
            self._events.handshake()
        self._handshake.release()

    # ----------------------------------------
    # HELLO
    # ----------------------------------------
    def _hello_handler(self, message):
        """ Получен ответ на hello """
        if self._handshake.is_running():
            self._handshake.on_message(message)
    # ----------------------------------------
    # ECHO
    # ----------------------------------------

    def _echo_handler(self, message):
        """ Получен ответ на echo """
        self._echo_sched.activity()

        if self._echo.is_running():
            self._echo.on_message(message)

    def _echo_send(self):
        """ Отправка echo если таймаут истек """
        if self._connector.ready() and self._echo_sched.expired():

            task = Task(TaskEcho,
                        self._connector,
                        self._echo_done)

            self._echo = TaskExecutor(self._timers,
                                      self._timeout,
                                      task)

    def _echo_done(self):
        """ Пришел ответ на echo или наступил таймаут"""
        task = self._echo.task.current
        success = task.success

        if not success:
            self._connector.disconnect()

        self._echo.release()

    # ----------------------------------------
    # Поиск тэгов
    # ----------------------------------------
    def find(self, tags):
        """ Поиск тэгов
        tags - список имен тэгов """
        if not self.ready():
            return False

        if self._find.is_running():
            return False

        task = Task(TaskFind,
                    self._connector,
                    tags,
                    self._find_done)
        self._find = TaskExecutor(self._timers,
                                  self._timeout,
                                  task)

        return True

    def _find_handler(self, message):
        """ Обработчик получения ответа на find """
        self._echo_sched.activity()
        if self._find.is_running():
            self._find.on_message(message)

    def _find_done(self):
        """ Поиск выполнен или наступил таймаут.
        На данном уровне не выполняется никаких проверок на полноту поиска.
        Этим занимается уровень поставивший задачу
        """
        task = self._find.task.current
        self._events.find(EventResult(
                          task.success,
                          confirmed=task.confirmed,
                          rejected=task.rejected))
        self._find.release()

    # ----------------------------------------
    # Подписка
    # ----------------------------------------
    def _subscribe_done(self):
        """ Подписка выполнена или наступил таймаут.
        На данном уровне не выполняется никаких проверок на полноту подписки.
        Этим занимается уровень поставивший задачу
        """
        task = self._subscribe.task.current
        self._events.subscribe(EventResult(
                               task.success,
                               confirmed=task.confirmed,
                               rejected=task.rejected))
        self._subscribe.release()

    # ----------------------------------------
    # Обновления тэга
    # ----------------------------------------
    def _update_handler(self, message):
        """ Обработчик сообщений об обновлении тэга """
        self._echo_sched.activity()
        if self._subscribe.is_running():
            self._subscribe.on_message(message)

        self._events.update(EventResult(
                            True,
                            tag=MessageHelper.get(message, 'tag'),
                            v=MessageHelper.get(message, 'v'),
                            t=MessageHelper.get(message, 't'),
                            q=MessageHelper.get(message, 'q')))
