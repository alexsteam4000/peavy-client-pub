""" Модуль для реализации таймеров на twisted """
from twisted.internet import task
from pv.client.timer import Timer


class TwistedTimerFactory:

    def __init__(self, factory, reactor):
        self._reactor = reactor
        factory.timer = self._create_timer
        factory.implementer = self

    def _create_timer(self, timeout, func, oneshot=True):
        """ создать таймер
        func - обработчик, который будет вызван по истечения timeout
        oneshot - True - таймер выполняется 1 раз"""
        if oneshot:
            timer = TwistedTimer(self._reactor, timeout, func)
        else:
            timer = TwistedLoop(self._reactor, timeout, func)

        return Timer(timer.is_active, timer.stop)


class TwistedTimer:
    """ Отложенный вызов """

    def __init__(self, reactor, timeout, func):
        self._timer = reactor.callLater(timeout, self.action)
        self._is_active = True
        self._action = func

    def stop(self):

        if self._is_active:
            self._timer.cancel()
            self._is_active = False

    def action(self):
        self._is_active = False
        self._action()

    def is_active(self):
        return self._is_active


class TwistedLoop:
    """ Циклический вызов """

    def __init__(self, reactor, timeout, func):
        self._loop = task.LoopingCall(func)
        self._loop.start(timeout)
        self._is_active = True

    def stop(self):

        if self._is_active:
            self._loop.stop()
            self._is_active = False

    def is_active(self):
        return self._is_active
