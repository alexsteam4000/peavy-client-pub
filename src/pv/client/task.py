""" Модуль для реализации задач клиента
Задача - неблокирующее действие, которое имеет лимит по времени.
При завершении задачи или наступелении таймаута вывзывающий код получит
уведомление.
Так же, возможно получение уведомлений о пром. состояниях, если задача
подразумевает множество шагов
"""
import uuid
import copy
from pv.client.timer import Timers
from pv.connector.message import MessageHelper, MessageType


class Task:
    """ Общее представление задачи """

    def __init__(self, task_type, *args):

        # Реализиция конкретных операций
        self.start = None
        self.stop = None
        self.on_message = None
        self.is_running = None
        self.current = task_type(self, *args)

        # проверим, чтобы все обработчики были заполнены
        assert self.start is not None
        assert self.stop is not None
        assert self.on_message is not None
        assert self.is_running is not None


class TaskExecutor:
    """ Класс для запуска задач и контроля времени выполнения """

    def __init__(self, factory, timeout, task):
        """
            factory - факта, занимающаяся созданием таймеров
            task - задача, которая д.б. выполнена
        """
        assert isinstance(factory, Timers)

        self._task = task
        self._timer = None
        self._expired = False

        # Запустить задачу и контр. таймер
        self._task.start()
        self._timer = factory.timer(timeout, self.stop)
        self._task = task

    def is_running(self):
        """ Задача выполняется """
        return not self._expired and self._task.is_running()

    @property
    def expired(self):
        """ Задача не была выполнена за отведенное время"""
        return self._expired

    def release(self):
        """ Перестать отслеживать задачу """
        self._timer.stop()
        self._task = TaskNoop()

    def stop(self):
        """ Остановить задачу """
        self._timer.stop()
        self._task.stop()
        self._task = TaskNoop()

    def on_message(self, message):
        """ Обработка сообщения """
        self._task.on_message(message)

    @property
    def task(self):
        return self._task


class SingleCall:
    """ Однократный вызов """

    def __init__(self, func):
        self.pulled = True
        self.function = func

    def __call__(self):
        if self.pulled:
            self.pulled = False
            self.function()


class TaskNoop:
    """ Пустая задача """

    def __init__(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def is_running(self):
        return False

    def on_message(self, message):
        pass

    def current(self):
        return 'Noop'


class TaskSubscribe:
    """ Подписка на тэги """

    def __init__(self, task, connector, tags, on_done):

        self._connector = connector
        self._tags = tags
        self._awaited = dict()
        self._confirmed = []
        self._rejected = copy.deepcopy(tags)
        self._on_done = SingleCall(on_done)

        task.start = self.start
        task.stop = self.stop
        task.on_message = self.on_message
        task.is_running = self.is_running

    def start(self):
        """ Запустить """
        if not self._connector.ready():
            return False

        for tag in self._tags:
            msg_id = str(uuid.uuid4())
            self._connector.subscribe(tag, ch=msg_id)
            self._awaited[msg_id] = tag

        return True

    def stop(self):
        """ Остановить """
        self._on_done()

    def is_running(self):
        """ Задача выполняется?"""
        return len(self._confirmed) < len(self._awaited)

    def on_message(self, message):
        """ Проанализировать полученное сообщение """
        msg_id = MessageHelper.get(message, 'ch')

        valid_id = msg_id in self._awaited
        if not valid_id:
            return

        name = self._awaited[msg_id]

        self._confirmed.append(name)
        self._rejected.remove(name)

        if len(self._confirmed) == len(self._awaited):
            self._on_done()

    @property
    def success(self):
        """ Флаг успешного выполнения """
        return len(self._confirmed) == len(self._tags)

    @property
    def confirmed(self):
        """ Список тэгов, подписка на которые была выполнена успешно """
        return self._confirmed

    @property
    def rejected(self):
        """ Список тэгов, подписка на которые была выполнена успешно """
        return self._rejected


class TaskFind:
    """ Поиск тэгов """

    def __init__(self, task, connector, tags, on_done):
        self._connector = connector
        self._tags = tags
        self._awaited = dict()
        self._confirmed = []
        self._rejected = []
        self._info = []
        self._on_done = SingleCall(on_done)

        task.start = self.start
        task.stop = self.stop
        task.on_message = self.on_message
        task.is_running = self.is_running

    def start(self):
        """ Запустить"""
        if not self._connector.ready():
            return False

        for tag in self._tags:
            msg_id = str(uuid.uuid4())
            self._connector.find(tag, ch=msg_id)
            self._awaited[msg_id] = tag

        return True

    def stop(self):
        """ Остановить"""
        self._on_done()

    def is_running(self):
        """ Задача выполняется ?"""
        return len(self._confirmed) + len(self._rejected) < len(self._awaited)

    def on_message(self, message):
        """ Проанализировать полученное сообщение """
        msg_id = MessageHelper.get(message, 'ch')

        valid_id = msg_id in self._awaited
        if not valid_id:
            return

        # должен быть корректный ответ
        valid = MessageHelper.command(message) == 'find_ok'
        name = self._awaited[msg_id]

        if valid:
            tag_type = MessageHelper.get(message, 'type')
            # запомнить информацию о посл. подтвержденном сообщении
            self._confirmed.append((name, tag_type))
        else:
            self._rejected.append((name, None))

        if len(self._confirmed) + len(self._rejected) == len(self._awaited):
            self._on_done()

    @property
    def success(self):
        """ Признак успешного завершения """
        return len(self._confirmed) == len(self._tags)

    @property
    def confirmed(self):
        """ Список найденных тэгов"""
        return self._confirmed

    @property
    def rejected(self):
        """ Список отклоненных тэгов"""
        return self._rejected


class TaskEcho:
    """ Отправка echo"""

    def __init__(self, task, connector, on_done):
        self._connector = connector
        self._wait_id = None
        self._received_id = None
        self._on_done = SingleCall(on_done)

        task.start = self.start
        task.stop = self.stop
        task.on_message = self.on_message
        task.is_running = self.is_running

    def start(self):
        """ Запустить """
        if not self._connector.ready():
            return False

        msg_id = str(uuid.uuid4())
        self._connector.echo(ch=msg_id)
        self._wait_id = msg_id

        return True

    def stop(self):
        """ Остановить """
        self._on_done()

    def is_running(self):
        """ Задача выполянеся ?"""
        return self._wait_id != self._received_id

    def on_message(self, message):
        """ Проанализировать сообщение """
        msg_id = MessageHelper.get(message, 'ch')
        if self._wait_id == msg_id:
            self._received_id = msg_id
            self._on_done()

    @property
    def success(self):
        """ Флаг успешного завершения"""
        return self._wait_id == self._received_id


class TaskHandshake:
    """ Рукопожатие
    hello (Request) -> welcome (Response)
    """

    def __init__(self, task, connector, on_done):
        self._connector = connector
        self._success = False
        self._on_done = SingleCall(on_done)

        task.start = self.start
        task.stop = self.stop
        task.on_message = self.on_message
        task.is_running = self.is_running

    def start(self):
        """ Запустить """
        if not self._connector.ready():
            return False

        self._connector.hello()
        return True

    def stop(self):
        """ Остановить """
        self._on_done()

    def is_running(self):
        """ Задача выполняется ? """
        return not self._success

    def on_message(self, message):
        """ Проанализировать сообщение """
        self._success = MessageHelper.type(message) == MessageType.HELLO
        self._on_done()

    @property
    def success(self):
        """ Флаг успешного завершения"""
        return self._success
