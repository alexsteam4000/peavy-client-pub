""" Модуль для работы с таймерами """


class Timer:

    def __init__(self, is_active, stop):
        """ """
        self.is_active = is_active
        self.stop = stop


class Timers:
    """ Класс для создания таймеров """

    def __init__(self, factory_type, *args):
        self.timer = None
        self.implementer = factory_type(self, *args)
