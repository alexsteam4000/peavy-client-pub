""" Трансортный уровень """


class TransportDummy:
    """ Пустой транспорт """

    def __init__(self, transport):

        transport.transport = self
        transport.start = self.start
        transport.stop = self.stop
        transport.write = self.write
        transport.close = self.close

    def start(self):
        pass

    def stop(self):
        pass

    def write(self, data):
        pass

    def close(self):
        pass


class TransportEvents:
    """ События на трансопртном уровне """

    def __init__(self):
        # Данные готовы для чтения.
        # 1 аргумент - данные
        self.read = None

        # Подключение выполнено
        # 0 аргументов
        self.connect = None

        # Подключение разорвано
        # 0 аргументов
        self.disconnect = None


class Transport:
    """ Связующее звено между верзним уровнем (коннектором) и низкоуровневыми
    функциями ввода-вывода. Требуется т.к. работу с сокетами можно организовать различными способами: родные
    либы питона, twisted, socketio и т.п. Каждый вариант со своими тараканами и своими подходами.
    Transport позволяет понизить уровень трэша, т.к. коннектор видит нижний уровнеь только
    через функции Transport, а нижний уровень общается с коннектором только через
    CB в Transport. соотсветсвенно задача НУ - заполнить реальными функциями поля write, close, ....
    Задача коннектора - заполнить CB
    """

    def __init__(self, transport_type, *args):

       # Обработчики событий, наступивших на тр. уровне
        # Вызываются со стороны транспорта.
        self.events = TransportEvents()

        # Операции с транспортным уровнем. Вызваются со стороны коннектора
        # Запись данных в транспортный уроваень.
        # 1 аргумент - данные
        self.write = None

        # Закрыть подключение (если оно поддерживается тр. уровнем)
        # 0 аргументов
        self.close = None

        # Запустить тр. уровень
        # 0 аргументов
        self.start = None

        # Остановть тр. уроваень
        # 0 аргументов
        self.stop = None
        self.connector = None  # экземлпяр коннектора

        # экземлпяр транспортного уровня
        self.transport = transport_type(self, *args)

    def ready(self):
        """ Признак готовноси """
        return self.connector and self.transport
