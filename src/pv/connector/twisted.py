""" Модуль для реализации транспортного уровня при помощи twisted """
import uuid
from twisted.internet import task
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.protocols.basic import LineReceiver

from pv.connector.base import Transport


class TCPChannel(LineReceiver):
    """ TCP-канал заточенный на получение/отправку текстовых данных
    Основная задача - получить уведомление о событи и передать на верх
    """

    def __init__(self, owner):
        self.delimiter = b'\n'
        self.owner = owner

    def write(self, data):
        """ Отправить данные """
        out = None
        if isinstance(data, str):
            out = data.encode('utf-8')
        elif isinstance(data, bytes):
            out = data

        if out:
            self.sendLine(out)

    def close(self):
        """ Закрыть """
        self.transport.loseConnection()

    def lineReceived(self, line):
        """ Получено сообщение """
        self.owner.on_read(line)

    def connectionMade(self):
        """ Подключение установлено """
        self.owner.on_connect(self)

    def connectionLost(self, reason):
        """ Подключение разорвано """
        self.owner.on_disconnect()


class TCPFactory(ReconnectingClientFactory):
    """ Факта для создания подключений с переподключением """

    def __init__(self, owner, max_delay=30):
        self.maxDelay = max_delay
        self.owner = owner

    def buildProtocol(self, addr):
        """ Все готово. Хотим новый экземпляр коннекта"""
        self.resetDelay()
        return TCPChannel(self.owner)

    def clientConnectionLost(self, connector, reason):
        """ Коннект разорван """
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        """ Попытка создать коннект провалена """
        ReconnectingClientFactory.clientConnectionFailed(self, connector,
                                                         reason)


class TwistedTransport:
    """ Движок на Twisted. Позволяет коннектору получать доступ к сети и таймерам.
    Уведомляет коннектор о событиях """

    def __init__(self, transport, reactor, host, port, reconnect_delay=30):

        assert isinstance(transport, Transport)

        # Привязать транспорт к реальным функциям для работы с сетью
        transport.transport = self
        transport.start = self.start
        transport.stop = self.stop
        transport.write = self.write
        transport.close = self.close

        self.transport = transport

        self._factory = TCPFactory(self, reconnect_delay)
        self._tcp = None
        self._host = host
        self._port = port
        self._reactor = reactor

    def on_read(self, data):
        """ В канале появились данные для чтения """
        self.transport.events.read(data)

    def on_connect(self, channel):
        """ Подключение установлено """
        self._tcp = channel
        self.transport.events.connect()

    def on_disconnect(self):
        """ Подключение разорвано """
        self._tcp = None
        self.transport.events.disconnect()

    def start(self):
        """ Запуск движка """
        self._reactor.connectTCP(self._host, self._port, self._factory)

    def stop(self):
        """ Останов """
        self.transport.close()
        self._factory.stopTrying()

    def write(self, data):
        """ Отправить сообщение """
        if self._tcp:
            self._tcp.write(data)

    def close(self):
        """ Закрыть подключение """
        if self._tcp:
            self._tcp.close()
            self._tcp = None
