""" Коннектор
Модуль обеспечивает базовый функционал при работу с сервером VC.
Базовый - отправка/прием сообщений, уведомление стороннего кода о
наступлении событий.
"""
from enum import Enum
from pv.connector.transport import Transport, TransportDummy
from pv.connector.message import MessageBuilder, MessageType, MessageHelper


class ConnectorConfig:
    """ Конфигурация коннектора """

    def __init__(self):

        def empty(arg=None):
            """ Затычка для обработчиков """
            pass

        self.name = 'name'
        self.host = '127.0.0.1'
        self.port = 8091
        self.check_connection_timeout = 20
        self.version = '0.2.0'
        self.transport = Transport(TransportDummy)
        self.events = ConnectorEvents()

        # Немного дефолтных обработчиков
        self.events.hello = empty
        self.events.echo = empty
        self.events.find = empty
        self.events.next = empty
        self.events.update = empty
        self.events.invalid_message = empty
        self.events.up = empty
        self.events.down = empty


class ConnectorEvents:
    """ События в коннекторе """

    def __init__(self):
        self.hello = None
        self.echo = None
        self.find = None
        self.update = None
        self.next = None
        self.up = None
        self.down = None
        self.invalid_message = None


class Connector:
    """ Коннектор. Позволяет получить доступ к VC из питона """

    def __init__(self, config):

        assert isinstance(config, ConnectorConfig)
        self._events = config.events
        self._name = config.name
        self._version = config.version
        self._ready = False

        # Заполнить обработчики для получения уведомлений от транспортного
        # уровня
        self._transport = config.transport
        self._transport.events.read = self.on_read
        self._transport.events.connect = self.on_connect
        self._transport.events.disconnect = self.on_disconnect
        self._transport.connector = self

    def start(self):
        """ Запустить коннектор """
        self._transport.start()

    def stop(self):
        self._transport.stop()

    def disconnect(self):
        """" Отключится от сервера """
        if self._transport.ready():
            self._transport.close()
            return True
        return False

    def hello(self):
        """ Отправить hello """
        if not self.ready():
            return False

        message = {'c': 'hello', 'name': self._name, 'protocol': self._version}
        self._write(message)
        return True

    def echo(self, **kwargs):
        """ Отправить echo """
        if not self.ready():
            return False

        message = MessageBuilder.echo(**kwargs)
        self._write(message)

        return True

    def subscribe(self, tag, tstamp=True, ref='name', **kwargs):
        """ Подписаться на тэг"""
        if not self.ready():
            return False

        message = MessageBuilder.subscribe(tag, tstamp, ref, **kwargs)
        self._write(message)

        return True

    def find(self, tag, tstamp=True, ref='name', **kwargs):
        """ Найти тэг"""
        if not self.ready():
            return False

        message = MessageBuilder.find(tag, tstamp, ref, **kwargs)
        self._write(message)

        return True

    def enumerate(self, tag=None, **kwargs):
        """ Получить первый/следующий тэг"""
        if not self.ready():
            return False

        message = MessageBuilder.enumerate(tag, **kwargs)
        self._write(message)

        return True

    def set(self, tag, value, quality, tstamp=None, **kwargs):
        """ Записать значение тэга"""
        if not self.ready():
            return False

        message = MessageBuilder.set(tag, value, quality, tstamp, **kwargs)
        self._write(message)

        return True

    def ready(self):
        """ Признак готовности"""
        return self._ready

    def on_connect(self):
        """ Связь установлена """
        self._ready = True
        self._events.up()

    def on_disconnect(self):
        """ Связь потеряна """
        self._ready = False
        self._events.down()

    def on_read(self, line):
        """ Входящее сообщение """
        # Пока с запасом

        try:
            message = MessageHelper.unpack(line)
            message_type = MessageHelper.type(message)

            if message_type == MessageType.HELLO:
                self._events.hello(message)
            elif message_type == MessageType.ECHO:
                self._events.echo(message)
            elif message_type == MessageType.FIND:
                self._events.find(message)
            elif message_type == MessageType.ENUMERATE:
                self._events.next(message)
            elif message_type == MessageType.UPDATE:
                self._events.update(message)

        except Exception as e:
            self._events.invalid_message(line)
            self._transport.close()

    def _write(self, data):
        """ Отправить сообщение """
        if self._transport.ready():
            self._transport.write(MessageHelper.pack(data))
