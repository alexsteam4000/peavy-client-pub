""" Модуль для работы с сообщениями VC"""
import json
from enum import Enum


class MessageType(Enum):
    """ Тип сообщения """
    HELLO = 1
    ECHO = 2
    FIND = 3
    ENUMERATE = 4
    UPDATE = 5


class MessageHelper:
    """ Набор вспомогательных функций для работы с сообщениями.
    """
    @staticmethod
    def command(message):
        """ Получить имя команды из сообщения """
        try:
            return message['c']
        except:
            pass
        return None

    @staticmethod
    def type(message):
        """ Получить тип сообщения """
        types = {
            'welcome': MessageType.HELLO,
            'echo': MessageType.ECHO,
            'find_ok': MessageType.FIND,
            'find_err': MessageType.FIND,
            'next_tag': MessageType.ENUMERATE,
            'next_end': MessageType.ENUMERATE,
            'next_err': MessageType.ENUMERATE,
            'upd': MessageType.UPDATE
        }
        return types[message['c']]

    @staticmethod
    def get(message, attrib):
        """ Получить значение аттрибута из сообщения """
        try:
            return message[attrib]
        except:
            pass
        return None

    @staticmethod
    def pack(message):
        """ Сообщение -> JSON """
        return json.dumps(message)

    @staticmethod
    def unpack(text):
        """JSON -> Сообщение """
        if isinstance(text, bytes):
            return json.loads(text.decode('utf-8'))
        return json.loads(text)


class MessageBuilder:
    @staticmethod
    def hello(self, name, **kwargs):
        """ Отправить hello """
        return {'c': 'hello', 'name': self.name, **kwargs}

    @staticmethod
    def echo(**kwargs):
        """ Отправить echo """
        return {'c': 'echo', **kwargs}

    @staticmethod
    def subscribe(tag, tstamp=True, ref='name', **kwargs):
        """ Подписаться на тэг """
        return {'c': 'sub', 'tag': tag, 'tstamp': tstamp, 'ref': ref, **kwargs}

    @staticmethod
    def find(tag, tstamp=True, ref='name', **kwargs):
        """ Найти тэг """
        return {'c': 'find', 'tag': tag, **kwargs}

    @staticmethod
    def enumerate(tag=None, **kwargs):
        """ Навигация по тэгам """
        if tag is None:
            return {'c': 'first', **kwargs}
        return {'c': 'next', 'tag': tag, **kwargs}

    @staticmethod
    def set(tag, value, quality, tstamp=None, **kwargs):
        """ Записать тэг """
        return {'c': 'set', 'tag': tag, 'v': value, 'q': quality, 't': tstamp,
                **kwargs}
