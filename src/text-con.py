
import argparse
from twisted.internet import reactor, stdio
from twisted.protocols.basic import LineReceiver

from pv.connector.twisted import TwistedTransport
from pv.connector.base import Connector, ConnectorConfig, Transport


class Commands(LineReceiver):
    """ Обработчик команд пользователя """

    def __init__(self, connector):
        super().__init__()
        self.connector = connector
        self.supported = {'hello': self.connector.hello,
                          'echo': self.connector.echo,
                          'find': self.connector.find,
                          'first': self.connector.enumerate,
                          'next': self.connector.enumerate,
                          'sub': self.connector.subscribe,
                          'set': self.connector.set,
                          'exit': self.exit,
                          'detach': self.detach,
                          'start': self.connector.start,
                          'stop': self.connector.stop,
                          }

    def connectionMade(self):
        """ Терминал запущеню Выводим приглашение """
        self.delimiter = b'\n'
        self.transport.write(b'>>> ')

    def lineReceived(self, line):
        """ Строка прочитана. Отрабатываем и выводим приглашение """
        self.execute_command(line)
        self.transport.write(b'>>> ')

    def list(self):
        """ Списко доступных команд """
        l = list()
        for k in self.supported.keys():
            l.append(k)

        return l

    def execute_command(self, line):
        """ Отработка команд
        Сводится к поиску в словаре поддерживаемых команд и вызову с аргументами
        пользовательского ввода
        """
        result = 0
        args = None
        tokens = line.split()
        if not tokens:
            return

        cmd = tokens[0].decode('utf-8')
        if len(tokens) > 1:
            args = list(map(lambda p: Commands.convert(p), tokens[1:]))

        if cmd not in self.supported:
            print('Unknown command')
            return

        try:
            result = self.supported[cmd](
                *args) if args else self.supported[cmd]()
        except Exception as e:
            print(e)

        print('Execute command with result: {0}'.format(result))

    @staticmethod
    def convert(text):
        """ Конвертация 'особых' слов
        К ним относятся true/false и числа
        """
        try:
            fp = text.find(b'.')
            if fp == -1:
                v = int(text)
            else:
                v = float(text)
            return v
        except:
            pass

        if text in [b'True', b'true']:
            return True

        if text in [b'False', b'false']:
            return False

        return text.decode('utf-8')

    def detach(self):
        """ Отсоединить терминал
        После отсоединения ввод новых команд будет невозможен.
        Позволяет избежать блокировки при переполнении stdin'а
        """
        self.transport.write(
            b'STDIN detached. Restart app to get opportunity to enter new commands\n')
        self.transport.loseConnection()
        return True

    def exit(self):
        """ Выйти """
        reactor.stop()
        return True


def main():
    """
    - разобрать аргументы
    - запустить коннектор
    - запустить работу с терминалом
    - запустить twisted
    """
    parser = argparse.ArgumentParser(description='VC text client')

    parser.add_argument('--host',
                        dest='host',
                        default='127.0.0.1',
                        help='VC address',
                        )
    parser.add_argument('--port',
                        dest='port',
                        type=int,
                        default=8091,
                        help='VC port',
                        )

    args = parser.parse_args()

    # Настройки коннектора
    config = ConnectorConfig()
    config.name = 'PeavyTextConnector'
    config.host = args.host
    config.port = args.port

    # Настройка транспортного уровня коннектора
    config.transport = Transport(
        TwistedTransport, reactor, config.host, config.port)

    # Настройка обработчиков событий
    config.events.hello = lambda msg: print('HELLO:{0}'.format(msg))
    config.events.echo = lambda msg: print('ECHO:{0}'.format(msg))
    config.events.find = lambda msg: print('FIND:{0}'.format(msg))
    config.events.next = lambda msg: print('NEXT:{0}'.format(msg))
    config.events.update = lambda msg: print('UPDATE:{0}'.format(msg))
    config.events.up = lambda: print('Link Up')
    config.events.down = lambda: print('Link Down')

    # Создание коннектора
    connector = Connector(config)

    # Работа с терминалом
    commands = Commands(connector)

    print("Supported commands:")
    for c in commands.list():
        print("\t {0}".format(c))

    stdio.StandardIO(commands)

    reactor.run()


if __name__ == '__main__':
    main()


# Пример работы

# >>> start
# Execute command with result: None
# >>> hello
# Execute command with result: True
# >>> HELLO:{'c': 'welcome'}
# >>> echo
# Execute command with result: 41eff7ab-1070-4d01-a705-02563857d407
# >>> ECHO:{'c': 'echo', 'ch': '41eff7ab-1070-4d01-a705-02563857d407'}
#
# >>> sub int
# Execute command with result: ef235a23-9ca6-4a14-9999-18502a6b3bae
# >>> UPDATE:{'c': 'upd', 'tag': 'int', 'v': 232, 'q': 0, 't': '2018-11-21T07:05:01.883074', 'ch': 'ef235a23-9ca6-4a14-9999-18502a6b3bae'}
#
# >>> set int 333 0
# Execute command with result: d002bd9d-ba9c-4271-977d-79aec7b00399
# >>> UPDATE:{'c': 'upd', 'tag': 'int', 'v': 333, 'q': 0, 't': '2018-11-21T07:14:35.246202', 'ch': 'ef235a23-9ca6-4a14-9999-18502a6b3bae'}
#
# >>> exit
# Execute command with result: True
# Link Down
